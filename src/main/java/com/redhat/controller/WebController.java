package com.redhat.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WebController {

	@GetMapping("/")
	public String greeting() {
		return "Hello Redhatter from Spring Boot";
	}
}